﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums_3
{
    class Program
    {
        //defining an enum without the flag attribute

        enum Without_Flag
        {
            //its almost always a good idea to have a value of 0 and tag it as None
            none = 0,
            phone = 1,
            computer = 2,
            laptop = 4,
            tablet = 8

            //do note that I am giving it values in multiples of 2 power 0.
            //so the first item phone is 2 power 0 = 1
            //computer is 2 power 1 = 2
            //laptop is 2 power 2 = 4 
            //tablet is 2 power 3 = 8
            //like this...why this is the case becomes more obvious below
        }

        //defining an enum with the flag attribute
        //I am reusing the exact same values from the above enum definition.
        [Flags]   //by inlcuding the flag attribute i have made the following enum be treated as flags
        enum With_Flag
        {
            //its almost always a good idea to have a value of 0 and tag it as None
            none = 0,
            phone = 1,
            computer = 2,
            laptop = 4,
            tablet = 8

            //do note that I am giving it values in multiples of 2 power 0.
            //so the first item phone is 2 power 0 = 1
            //computer is 2 power 1 = 2
            //laptop is 2 power 2 = 4 
            //tablet is 2 power 3 = 8
            //like this...why this is the case becomes more obvious below
        }

        static void Main(string[] args)
        {
            //now I will simply loop through each of the enum values
            //and then display them.
            //just before displaying, I will cast them 

            for(int i = 0;i<=16;i++)
            {
                //as promised earlier, casting the i to enum Without_Flag
                Console.WriteLine(" {0} - {1} ", i, (Without_Flag)i);
            }

            for (int i = 0; i <=16; i++)
            {
                //as promised earlier, casting the i to enum Without_Flag
                Console.WriteLine(" {0} - {1} ", i, (With_Flag)i);
            }

            //this is keep the cosole window from vanishing
            Console.ReadLine();

            //read the following comments after having observerd that outpu that comes in your 
            //visual studio console window

            //now, you will notice that the output for the without flag has a proper output, only five times. 
            //that is when the i value is able to match up with exact values in the enum

            //now, when you look at the portion of the output for the enum with flag enabled
            //you will notice that the flag attribute ensures that the cast works for combination values as well
            //for example, when i is 3, the flag less enum shows nothing. it just shows 3.
            ///however, when i is 3, the flag full enum shows black and red
            //as per the enum definition, black has a value of 1, and red has 2. add them together, it means 3

            //so, thanks to the enum attribute, the casting is able to work out that, although there is no direct cast
            //available for 3, it can be 'represented' by using two other representations 'black' and 'red'.

            //when you think about it, this is the point of enums. to represent values, usually with their string equavalents.
            //further, earlier, I told how the values are better off as power of 2.

            //if we had given values as 1, 2, 3 and so on, at some point a confusion will arise.
            //for instance, 5 and 1 is 6, and so is 4 and 2. you cannot work like that. 

            //however, when you use explicity power of 2 values, for every value, there is only be one combination and 
            //one combination only.
        }
    }
}
